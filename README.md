# OpenGL texture mapping demo

This is a small educational program to demonstrate how OpenGL handles texture wrapping modes. 

Other graphics APIs will behave in very similar ways, 
although they may use the terms `u` and `v` instead of `s` and `t` to refer to texture coordinates.


<p float="left">
  <img src="/pic0.png" width="45%" /> 
  &nbsp; &nbsp;
  <img src="/pic1.png" width="45%" />
</p>

## Dependencies
* OpenGL 1.2
* GLFW 3

## Build
```console
./build.sh
./texture-demo
```

## Controls
* `WASD` to move the texture coordinates around.
* Press `h` to see a complete list of controls.

## Modifying
Play around with [texture-demo.c](./texture-demo.c) and see what you can come up with. 

In particular, see lines `294 to 324` for the texture drawing code.

```c
// To replicate pic1.png, set s0 and t0 to the following values
float s0 = -1 + TextureOffset.offs_s;
float t0 = -1 + TextureOffset.offs_t;
```


## Credits
*awesomeface.png is is from [learnopengl.com](https://learnopengl.com)*


## References
[STB Libraries (Github)](https://github.com/nothings/stb)

[OpenGL Documentation](https://docs.gl)

[GLFW 3 Documentation](https://www.glfw.org/docs/latest/)

[learnopengl.com](https://learnopengl.com)






#ifndef _IMMF_H_
#define _IMMF_H_


// An immediate-mode OpenGL bitmap font renderer using the legacy API
// Example usage in code

#if 0
	// Allocate texture memory in OpenGL for each glyph, where each texture is 16x16 pixels
	immf_Init(16 ,16);

	// Dimensions of screen space
	immf_WindowDimensions(800, 600);

	// y=0 is now the top of the screen, as opposed to OpenGL's approach
	immf_VerticalFlip(1);

	// Draw a string at x=20, y=20, where each quad is 16x16 pixels and colour is 0xFFFFFFFF (white)
	// Colour is RGBA little-endian. Alpha blending is supported
	immf_DrawString(20, 20, 16, 16, 0xFFFFFFFF, "Hello, world!");

	// In red
	immf_DrawString(20, 40, 16, 16, 0xFF0000FF, "Hello, world!");

	// In white, with 0xAA for alpha
	immf_DrawString(20, 60, 16, 16, 0xAAFFFFFF, "Hello, world!");

#endif

/*
	++++++++++++++++++++++++++++++++
	README:
	++++++++++++++++++++++++++++++++

	This is an stb-style library and follows similar conventions

	Define IMMF_IMPLEMENTATION  `once` in a single source file before including this header

	Please do note that this library is far from efficient
	In addition, we don't support unicode at all
	Its main goal is ease of use and quick integration

	Here are some strategies to increase performance:

	Use a texture atlas with pre-generated UV coordinates
	Right now we allocate and bind a separate texture for each glyph, which is dumb
	The texture atlas and coordinates can be generated in immf_Init

	Batch rendering
	Legacy (1.x):
	glVertexPointer and glColorPointer

	In modern OpenGL (3.x):
	glVertexAttribPointer and glBufferData with VBOs

	In even more modern OpenGL (4.x):
	Given x, y, width, height, and color, generate quad vertex coordinates in the vertex shader itself to minimise data upload size
	Otherwise, we would have to send 4x more data using the above methods
	I tried this with the new shader storage buffer objects and it seemed to work. There may be other ways of doing the same thing

	This will also require introducing some sort of buffer system as well as a flush call at the end of the frame
	to submit the buffer

	Final notes:
	This library is heavily biased towards OpenGL, which is not exactly ideal for widespread use
	Perhaps a future version will abstract the draw calls and texture allocations
	to be used in a more generic way

	This library has only been tested on Linux, with GCC and Clang compilers
	Most of the code should be cross platform but I make no guarantees that it will compile
*/


#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h> 		// size_t, vsnprintf
#include <stdarg.h> 	// va_*
#include <stdint.h> 	// uint*
#include <assert.h> 	// assert
#include <malloc.h> 	// calloc, free

#include <GL/gl.h>

#define immf__u8 uint8_t
#define immf__u32 uint32_t


#define IMMF_GLYPH_BPP 4
#define IMMF_GLYPHS_MAX 512
#define IMMF_DRAW_STRING_BUF_MAX 2048
#define IMMF_ATTRIB_STACK_SIZE 64

#define IMMF_GLYPH_BEGIN 0x20
#define IMMF_GLYPH_END 0x7F

/**********************************
* Types
**********************************/
typedef struct
{
	immf__u32 *pixels;
	immf__u32 sizeInBytes;
	immf__u32 w;
	immf__u32 h;

	GLuint gl_TexId;
} immf_glyph;

typedef struct
{
	immf_glyph Glyph[IMMF_GLYPHS_MAX];
} immf_glyphs;

typedef enum
{
	IMMF_FILTER_MODE_DUMMY,
	IMMF_FILTER_MODE_NONE,
	IMMF_FILTER_MODE_BILINEAR,
} immf_filter_mode;

typedef enum
{
	IMMF_ATTRIB_SPACING_VERT,
	IMMF_ATTRIB_WINDOW_DIMENSIONS,
} immf_attrib_type;

typedef struct
{
	immf_attrib_type Type;
	union
	{
		float spacing;

		struct
		{
			int winWidth;
			int winHeight;
		};
	};
} immf_attrib;

typedef struct
{
	immf_attrib AttribStack[IMMF_ATTRIB_STACK_SIZE];
	int sp;
} immf_attrib_stack;

typedef struct
{
	float hspacing;
	float vspacing;
	immf__u32 WindowWidth;
	immf__u32 WindowHeight;
	int wordWrap;
	float MatrixWidth;
	float MatrixHeight;
	int VerticalFlip;
	immf_filter_mode FilterMode;

	int Initialised;
} immf_context;


/**********************************
* Declarations
**********************************/
#ifndef IMMF_DEF
#define IMMF_DEF extern
#endif

IMMF_DEF void immf_Init(immf__u32 GlyphWidth, immf__u32 GlyphHeight);
/*
   Important!
   Call this to allocate texture memory for OpenGL
   Without this call, this library effectively does nothing

	Caveat: Because we use an 8x8 bitmap, this call only supports dimensions that are multiples of 8
*/

IMMF_DEF void immf_WindowDimensions(immf__u32 width, immf__u32 height);
/*
   Set the orthographic projection matrix for the window
   This library operates in 2D space
   Must be called at least once before calling immf_DrawString
*/

IMMF_DEF void immf_DrawStringBase(float x, float y, float w, float h, immf__u32 color, const char *text, const immf__u32 len);
IMMF_DEF void immf_DrawString(float x, float y, float w, float h, immf__u32 color, const char *fmt, ...);
/*
   The only functions you will need to call once you initialise the library
   for doing basic font rendering

   immf_DrawStringBase is where all of the work gets done
   immf_DrawString is a convenience wrapper for printf style format strings
*/


IMMF_DEF void immf_PushAttrib(immf_attrib_type Type);
IMMF_DEF void immf_PopAttrib();
/*
   Set various attributes before calling immf_DrawString
   Call immf_PopAttrib to restore the previous state of the context

*/

IMMF_DEF void immf_FontSpacingHorz(float value);
IMMF_DEF void immf_FontSpacingVert(float value);
/*
   Spacing in pixels between each quad
   Defaults to 0 if not called
*/


IMMF_DEF void immf_WordWrapEnable();
IMMF_DEF void immf_WordWrapDisable();
/*
   Enable / disable word wrap
   This does not account for client side window changes
   The word wrap is calculated purely based on the pixel coordinates of the projection matrix and word length
*/

IMMF_DEF void immf_VerticalFlip(int flip);
/*
   Configure the coordinate system used by this library
   If true, y=0 is the top left of the screen
   If false, y=0 is the bottom left of the screen
*/

IMMF_DEF void immf_FilterMode(immf_filter_mode Mode);
/*
   Nearest filtering, or linear filtering
   Note that to get decent results with linear filtering,
   call immf_Init with at least 16x16 dimensions

   With nearest filtering it doesn't matter
*/



#ifdef IMMF_IMPLEMENTATION

/**
 * 8x8 monochrome bitmap fonts for rendering
 * Author: Daniel Hepper <daniel@hepper.net>
 *
 * License: Public Domain
 *
 * Based on:
 * // Summary: font8x8.h
 * // 8x8 monochrome bitmap fonts for rendering
 * //
 * // Author:
 * //     Marcel Sondaar
 * //     International Business Machines (public domain VGA fonts)
 * //
 * // License:
 * //     Public Domain
 *
 * Fetched from: http://dimensionalrift.homelinux.net/combuster/mos3/?p=viewsource&file=/modules/gfx/font8_8.asm
 **/

// Constant: font8x8_basic
// Contains an 8x8 font map for unicode points U+0000 - U+007F (basic latin)
const unsigned char font8x8_basic[128][8] = {
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0000 (nul)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0001
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0002
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0003
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0004
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0005
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0006
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0007
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0008
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0009
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000A
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000B
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000C
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000D
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000E
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+000F
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0010
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0011
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0012
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0013
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0014
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0015
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0016
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0017
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0018
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0019
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001A
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001B
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001C
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001D
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001E
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+001F
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0020 (space)
    { 0x18, 0x3C, 0x3C, 0x18, 0x18, 0x00, 0x18, 0x00},   // U+0021 (!)
    { 0x36, 0x36, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0022 (")
    { 0x36, 0x36, 0x7F, 0x36, 0x7F, 0x36, 0x36, 0x00},   // U+0023 (#)
    { 0x0C, 0x3E, 0x03, 0x1E, 0x30, 0x1F, 0x0C, 0x00},   // U+0024 ($)
    { 0x00, 0x63, 0x33, 0x18, 0x0C, 0x66, 0x63, 0x00},   // U+0025 (%)
    { 0x1C, 0x36, 0x1C, 0x6E, 0x3B, 0x33, 0x6E, 0x00},   // U+0026 (&)
    { 0x06, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0027 (')
    { 0x18, 0x0C, 0x06, 0x06, 0x06, 0x0C, 0x18, 0x00},   // U+0028 (()
    { 0x06, 0x0C, 0x18, 0x18, 0x18, 0x0C, 0x06, 0x00},   // U+0029 ())
    { 0x00, 0x66, 0x3C, 0xFF, 0x3C, 0x66, 0x00, 0x00},   // U+002A (*)
    { 0x00, 0x0C, 0x0C, 0x3F, 0x0C, 0x0C, 0x00, 0x00},   // U+002B (+)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x0C, 0x06},   // U+002C (,)
    { 0x00, 0x00, 0x00, 0x3F, 0x00, 0x00, 0x00, 0x00},   // U+002D (-)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x0C, 0x00},   // U+002E (.)
    { 0x60, 0x30, 0x18, 0x0C, 0x06, 0x03, 0x01, 0x00},   // U+002F (/)
    { 0x3E, 0x63, 0x73, 0x7B, 0x6F, 0x67, 0x3E, 0x00},   // U+0030 (0)
    { 0x0C, 0x0E, 0x0C, 0x0C, 0x0C, 0x0C, 0x3F, 0x00},   // U+0031 (1)
    { 0x1E, 0x33, 0x30, 0x1C, 0x06, 0x33, 0x3F, 0x00},   // U+0032 (2)
    { 0x1E, 0x33, 0x30, 0x1C, 0x30, 0x33, 0x1E, 0x00},   // U+0033 (3)
    { 0x38, 0x3C, 0x36, 0x33, 0x7F, 0x30, 0x78, 0x00},   // U+0034 (4)
    { 0x3F, 0x03, 0x1F, 0x30, 0x30, 0x33, 0x1E, 0x00},   // U+0035 (5)
    { 0x1C, 0x06, 0x03, 0x1F, 0x33, 0x33, 0x1E, 0x00},   // U+0036 (6)
    { 0x3F, 0x33, 0x30, 0x18, 0x0C, 0x0C, 0x0C, 0x00},   // U+0037 (7)
    { 0x1E, 0x33, 0x33, 0x1E, 0x33, 0x33, 0x1E, 0x00},   // U+0038 (8)
    { 0x1E, 0x33, 0x33, 0x3E, 0x30, 0x18, 0x0E, 0x00},   // U+0039 (9)
    { 0x00, 0x0C, 0x0C, 0x00, 0x00, 0x0C, 0x0C, 0x00},   // U+003A (:)
    { 0x00, 0x0C, 0x0C, 0x00, 0x00, 0x0C, 0x0C, 0x06},   // U+003B (;)
    { 0x18, 0x0C, 0x06, 0x03, 0x06, 0x0C, 0x18, 0x00},   // U+003C (<)
    { 0x00, 0x00, 0x3F, 0x00, 0x00, 0x3F, 0x00, 0x00},   // U+003D (=)
    { 0x06, 0x0C, 0x18, 0x30, 0x18, 0x0C, 0x06, 0x00},   // U+003E (>)
    { 0x1E, 0x33, 0x30, 0x18, 0x0C, 0x00, 0x0C, 0x00},   // U+003F (?)
    { 0x3E, 0x63, 0x7B, 0x7B, 0x7B, 0x03, 0x1E, 0x00},   // U+0040 (@)
    { 0x0C, 0x1E, 0x33, 0x33, 0x3F, 0x33, 0x33, 0x00},   // U+0041 (A)
    { 0x3F, 0x66, 0x66, 0x3E, 0x66, 0x66, 0x3F, 0x00},   // U+0042 (B)
    { 0x3C, 0x66, 0x03, 0x03, 0x03, 0x66, 0x3C, 0x00},   // U+0043 (C)
    { 0x1F, 0x36, 0x66, 0x66, 0x66, 0x36, 0x1F, 0x00},   // U+0044 (D)
    { 0x7F, 0x46, 0x16, 0x1E, 0x16, 0x46, 0x7F, 0x00},   // U+0045 (E)
    { 0x7F, 0x46, 0x16, 0x1E, 0x16, 0x06, 0x0F, 0x00},   // U+0046 (F)
    { 0x3C, 0x66, 0x03, 0x03, 0x73, 0x66, 0x7C, 0x00},   // U+0047 (G)
    { 0x33, 0x33, 0x33, 0x3F, 0x33, 0x33, 0x33, 0x00},   // U+0048 (H)
    { 0x1E, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+0049 (I)
    { 0x78, 0x30, 0x30, 0x30, 0x33, 0x33, 0x1E, 0x00},   // U+004A (J)
    { 0x67, 0x66, 0x36, 0x1E, 0x36, 0x66, 0x67, 0x00},   // U+004B (K)
    { 0x0F, 0x06, 0x06, 0x06, 0x46, 0x66, 0x7F, 0x00},   // U+004C (L)
    { 0x63, 0x77, 0x7F, 0x7F, 0x6B, 0x63, 0x63, 0x00},   // U+004D (M)
    { 0x63, 0x67, 0x6F, 0x7B, 0x73, 0x63, 0x63, 0x00},   // U+004E (N)
    { 0x1C, 0x36, 0x63, 0x63, 0x63, 0x36, 0x1C, 0x00},   // U+004F (O)
    { 0x3F, 0x66, 0x66, 0x3E, 0x06, 0x06, 0x0F, 0x00},   // U+0050 (P)
    { 0x1E, 0x33, 0x33, 0x33, 0x3B, 0x1E, 0x38, 0x00},   // U+0051 (Q)
    { 0x3F, 0x66, 0x66, 0x3E, 0x36, 0x66, 0x67, 0x00},   // U+0052 (R)
    { 0x1E, 0x33, 0x07, 0x0E, 0x38, 0x33, 0x1E, 0x00},   // U+0053 (S)
    { 0x3F, 0x2D, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+0054 (T)
    { 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x3F, 0x00},   // U+0055 (U)
    { 0x33, 0x33, 0x33, 0x33, 0x33, 0x1E, 0x0C, 0x00},   // U+0056 (V)
    { 0x63, 0x63, 0x63, 0x6B, 0x7F, 0x77, 0x63, 0x00},   // U+0057 (W)
    { 0x63, 0x63, 0x36, 0x1C, 0x1C, 0x36, 0x63, 0x00},   // U+0058 (X)
    { 0x33, 0x33, 0x33, 0x1E, 0x0C, 0x0C, 0x1E, 0x00},   // U+0059 (Y)
    { 0x7F, 0x63, 0x31, 0x18, 0x4C, 0x66, 0x7F, 0x00},   // U+005A (Z)
    { 0x1E, 0x06, 0x06, 0x06, 0x06, 0x06, 0x1E, 0x00},   // U+005B ([)
    { 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x40, 0x00},   // U+005C (\)
    { 0x1E, 0x18, 0x18, 0x18, 0x18, 0x18, 0x1E, 0x00},   // U+005D (])
    { 0x08, 0x1C, 0x36, 0x63, 0x00, 0x00, 0x00, 0x00},   // U+005E (^)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF},   // U+005F (_)
    { 0x0C, 0x0C, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+0060 (`)
    { 0x00, 0x00, 0x1E, 0x30, 0x3E, 0x33, 0x6E, 0x00},   // U+0061 (a)
    { 0x07, 0x06, 0x06, 0x3E, 0x66, 0x66, 0x3B, 0x00},   // U+0062 (b)
    { 0x00, 0x00, 0x1E, 0x33, 0x03, 0x33, 0x1E, 0x00},   // U+0063 (c)
    { 0x38, 0x30, 0x30, 0x3e, 0x33, 0x33, 0x6E, 0x00},   // U+0064 (d)
    { 0x00, 0x00, 0x1E, 0x33, 0x3f, 0x03, 0x1E, 0x00},   // U+0065 (e)
    { 0x1C, 0x36, 0x06, 0x0f, 0x06, 0x06, 0x0F, 0x00},   // U+0066 (f)
    { 0x00, 0x00, 0x6E, 0x33, 0x33, 0x3E, 0x30, 0x1F},   // U+0067 (g)
    { 0x07, 0x06, 0x36, 0x6E, 0x66, 0x66, 0x67, 0x00},   // U+0068 (h)
    { 0x0C, 0x00, 0x0E, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+0069 (i)
    { 0x30, 0x00, 0x30, 0x30, 0x30, 0x33, 0x33, 0x1E},   // U+006A (j)
    { 0x07, 0x06, 0x66, 0x36, 0x1E, 0x36, 0x67, 0x00},   // U+006B (k)
    { 0x0E, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1E, 0x00},   // U+006C (l)
    { 0x00, 0x00, 0x33, 0x7F, 0x7F, 0x6B, 0x63, 0x00},   // U+006D (m)
    { 0x00, 0x00, 0x1F, 0x33, 0x33, 0x33, 0x33, 0x00},   // U+006E (n)
    { 0x00, 0x00, 0x1E, 0x33, 0x33, 0x33, 0x1E, 0x00},   // U+006F (o)
    { 0x00, 0x00, 0x3B, 0x66, 0x66, 0x3E, 0x06, 0x0F},   // U+0070 (p)
    { 0x00, 0x00, 0x6E, 0x33, 0x33, 0x3E, 0x30, 0x78},   // U+0071 (q)
    { 0x00, 0x00, 0x3B, 0x6E, 0x66, 0x06, 0x0F, 0x00},   // U+0072 (r)
    { 0x00, 0x00, 0x3E, 0x03, 0x1E, 0x30, 0x1F, 0x00},   // U+0073 (s)
    { 0x08, 0x0C, 0x3E, 0x0C, 0x0C, 0x2C, 0x18, 0x00},   // U+0074 (t)
    { 0x00, 0x00, 0x33, 0x33, 0x33, 0x33, 0x6E, 0x00},   // U+0075 (u)
    { 0x00, 0x00, 0x33, 0x33, 0x33, 0x1E, 0x0C, 0x00},   // U+0076 (v)
    { 0x00, 0x00, 0x63, 0x6B, 0x7F, 0x7F, 0x36, 0x00},   // U+0077 (w)
    { 0x00, 0x00, 0x63, 0x36, 0x1C, 0x36, 0x63, 0x00},   // U+0078 (x)
    { 0x00, 0x00, 0x33, 0x33, 0x33, 0x3E, 0x30, 0x1F},   // U+0079 (y)
    { 0x00, 0x00, 0x3F, 0x19, 0x0C, 0x26, 0x3F, 0x00},   // U+007A (z)
    { 0x38, 0x0C, 0x0C, 0x07, 0x0C, 0x0C, 0x38, 0x00},   // U+007B ({)
    { 0x18, 0x18, 0x18, 0x00, 0x18, 0x18, 0x18, 0x00},   // U+007C (|)
    { 0x07, 0x0C, 0x0C, 0x38, 0x0C, 0x0C, 0x07, 0x00},   // U+007D (})
    { 0x6E, 0x3B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   // U+007E (~)
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // U+007F
};


/**********************************
* Globals
**********************************/
immf_glyphs immf_GlobalGlyphs = {};
immf_context immf_GlobalImmFontContext = {};
immf_attrib_stack immf_GlobalImmAttribStack = {};

/**********************************
* Functions
**********************************/
void immf_PushAttrib(immf_attrib_type Type)
{
	if(immf_GlobalImmAttribStack.sp < IMMF_ATTRIB_STACK_SIZE)
	{
		int sp = immf_GlobalImmAttribStack.sp;
		immf_attrib *Attrib = &immf_GlobalImmAttribStack.AttribStack[sp];
		immf_GlobalImmAttribStack.sp++;
		switch(Type)
		{
			case IMMF_ATTRIB_SPACING_VERT:
				Attrib->spacing = immf_GlobalImmFontContext.vspacing;
				break;
			case IMMF_ATTRIB_WINDOW_DIMENSIONS:
				Attrib->winWidth = immf_GlobalImmFontContext.WindowWidth;
				Attrib->winHeight = immf_GlobalImmFontContext.WindowHeight;
				break;
			default:
				break;
		}
	}
}

void immf_PopAttrib()
{
	for(int i = 0; i < immf_GlobalImmAttribStack.sp; i++)
	{
		immf_attrib *Attrib = &immf_GlobalImmAttribStack.AttribStack[i];
		switch(Attrib->Type)
		{
			case IMMF_ATTRIB_SPACING_VERT:
				immf_GlobalImmFontContext.vspacing = Attrib->spacing;
				break;
			case IMMF_ATTRIB_WINDOW_DIMENSIONS:
				immf_GlobalImmFontContext.WindowWidth = Attrib->winWidth;
				immf_GlobalImmFontContext.WindowHeight = Attrib->winHeight;
				break;
			default:
				break;
		}
	}
}

void immf_FontSpacingHorz(float value)
{
	immf_GlobalImmFontContext.hspacing = value;
}

void immf_FontSpacingVert(float value)
{
	immf_GlobalImmFontContext.vspacing = value;
}

void immf_WindowDimensions(immf__u32 width, immf__u32 height)
{
	immf_GlobalImmFontContext.WindowWidth = width;
	immf_GlobalImmFontContext.WindowHeight = width;
	immf_GlobalImmFontContext.MatrixWidth = 2.0f / (float)width;
	immf_GlobalImmFontContext.MatrixHeight = 2.0f / (float)height;
}

int immf_ShouldWrap(immf__u32 x)
{
	int result = 0;

	if(immf_GlobalImmFontContext.wordWrap)
	{
		immf__u32 w = immf_GlobalImmFontContext.WindowWidth;
		if(w && x > w) result = 1;
	}

	return result;
}

void immf_WordWrapEnable()
{
	immf_GlobalImmFontContext.wordWrap = 1;
}

void immf_WordWrapDisable()
{
	immf_GlobalImmFontContext.wordWrap = 0;
}

void immf_VerticalFlip(int flip)
{
	if(flip) immf_GlobalImmFontContext.VerticalFlip = 1;
	else immf_GlobalImmFontContext.VerticalFlip = 0;
}

void immf_FilterMode(immf_filter_mode Mode)
{
	immf_filter_mode Result;
	immf_filter_mode Last;

	Last = immf_GlobalImmFontContext.FilterMode;
	if(Mode < IMMF_FILTER_MODE_NONE) Result = IMMF_FILTER_MODE_NONE;
	else Result = Mode;

	if(Result == Last) return;

	if(immf_GlobalImmFontContext.Initialised)
	{
		int gl_FilterMode = Result == IMMF_FILTER_MODE_NONE ? GL_NEAREST : GL_LINEAR;
		for(int i = 0; i < IMMF_GLYPHS_MAX; i++)
		{
			immf_glyph *Glyph = &immf_GlobalGlyphs.Glyph[i];
			if(Glyph->gl_TexId)
			{
				glBindTexture(GL_TEXTURE_2D, Glyph->gl_TexId);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, gl_FilterMode);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, gl_FilterMode);
			}
		}
	}

	immf_GlobalImmFontContext.FilterMode = Result;
}

void _immf_gl_GlyphGen(immf_glyph *Glyph)
{
	int FilterMode = (immf_GlobalImmFontContext.FilterMode == IMMF_FILTER_MODE_NONE) ? GL_NEAREST : GL_LINEAR;
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &Glyph->gl_TexId);
	glBindTexture(GL_TEXTURE_2D, Glyph->gl_TexId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Glyph->w, Glyph->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, Glyph->pixels);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}


void immf_Init(immf__u32 GlyphWidth, immf__u32 GlyphHeight)
{
	// We are using an 8 by 8 font
	assert(GlyphWidth % 8 == 0);
	assert(GlyphHeight % 8 == 0);

	int i;
	immf__u32 whiteness = 0xFF;
	immf__u32 blackness = 0x00;
	const immf__u32 bytesPerLogicalPixel = (GlyphWidth * IMMF_GLYPH_BPP) / 8;

	// Destination for texture
	// We will support RGB and RGBA for now

	for(i = IMMF_GLYPH_BEGIN; i < IMMF_GLYPH_END; i++)
	{
		immf_glyph *Glyph = &immf_GlobalGlyphs.Glyph[i];
		Glyph->w = GlyphWidth;
		Glyph->h = GlyphHeight;

		size_t toAlloc = GlyphWidth * GlyphHeight * sizeof *Glyph->pixels;
		Glyph->pixels = (immf__u32 *)calloc(toAlloc, 1);
		assert(Glyph->pixels);

		for(size_t i = 0; i < toAlloc / sizeof *Glyph->pixels; i++)
		{
			Glyph->pixels[i] = 0;
		}

		immf__u8 *drow = (immf__u8 *)Glyph->pixels;

		immf__u32 pitch = GlyphWidth * IMMF_GLYPH_BPP * (GlyphWidth / 8);
		// To the next row in the bitmap

		// Read from bitmap
		// Each set bit in the bitmap corresponds to a region of the texture

		// For each row
		for(int bitmapIndex = 0; bitmapIndex < 8; bitmapIndex++)
		{
			// For each col
			for(int bit = 0; bit < 8; bit++)
			{
				char pixel = (font8x8_basic[i][bitmapIndex] >> bit) & 0x1;
				char pixelColor;
				if(pixel)
				{
					pixelColor = whiteness;
					// putchar('#');
				}
				else
				{
					pixelColor = blackness;
					// putchar(' ');
				}

				immf__u8 *start = &drow[bytesPerLogicalPixel * bit];
				// Start of the texture memory corresponding to the logical pixel

				for(int dy = 0; dy < GlyphWidth / 8; dy++)
				{
					for(int dx = 0; dx < bytesPerLogicalPixel; dx++)
					{
						start[dx] = pixelColor;
					}
					start += GlyphWidth * IMMF_GLYPH_BPP;
				}
			}
			drow += pitch;
			// putchar('\n');
		}

		// We just generated data for a letter
		// Let's create a GL texture for it
		_immf_gl_GlyphGen(Glyph);
		free(Glyph->pixels);
	}

	immf_GlobalImmFontContext.Initialised = 1;
}

void _immf_gl_DrawGlyph(float x, float y, float w, float h, immf__u32 texid)
{
	glBindTexture(GL_TEXTURE_2D, texid);
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);

	glTexCoord2f(0, 1);
	glVertex2f(x, y + h);

	glTexCoord2f(1, 1);
	glVertex2f(x + w, y + h);

	glTexCoord2f(1, 0);
	glVertex2f(x + w, y);

	glEnd();
}

void immf_gl_DrawQuad(float x, float y, float w, float h, immf__u32 color)
{
	// Color is RGBA (R is LSB)
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	float a = immf_GlobalImmFontContext.MatrixWidth;
	float f = immf_GlobalImmFontContext.MatrixHeight;

	if(immf_GlobalImmFontContext.VerticalFlip) f = -f;

	float m[16] =
	{
		a, 0, 0, 0,
		0, f, 0, 0,
		0, 0, 1, 0,
		-1, 1, 1, 1,
	};

	glPushMatrix();
	glLoadMatrixf(m);


	glColor4ubv((immf__u8 *)&color);

	glBegin(GL_QUADS);

	glVertex2f(x, y);
	glVertex2f(x, y + h);
	glVertex2f(x + w, y + h);
	glVertex2f(x + w, y);

	glEnd();

	glPopMatrix();
	glPopAttrib();
}

void immf_DrawQuad(float x, float y, float w, float h, immf__u32 color)
{
	immf_gl_DrawQuad(x, y, w, h, color);
}

void immf_DrawStringBase(float x, float y, float w, float h, immf__u32 color, const char *text, const immf__u32 len)
{
	float dx = 0;
	float dy = 0;

	// This is so we can render fonts regardless of how glOrtho is set up
	// Requires calling immf_WindowDimensions to set the desired render dimensions
	float a = immf_GlobalImmFontContext.MatrixWidth;
	float f = immf_GlobalImmFontContext.MatrixHeight;

	if(immf_GlobalImmFontContext.VerticalFlip) f = -f;

	float m[16] =
	{
		a, 0, 0, 0,
		0, f, 0, 0,
		0, 0, 1, 0,
		-1, 1, 1, 1,
	};

	glPushMatrix();
	glLoadMatrixf(m);

	// Note: PushAttrib slows things down quite a bit
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Color is RGBA (R is LSB)
	glColor4ubv((immf__u8 *)&color);

	// Glyphs
	char c;
	for(immf__u32 i = 0; i < len; i++)
	{
		c = text[i];
		if(c == '\n')
		{
			dy += h + immf_GlobalImmFontContext.vspacing;
			dx = 0;
			continue;
		}

		if(immf_ShouldWrap(x + dx + w + immf_GlobalImmFontContext.hspacing))
		{
			dy += h + immf_GlobalImmFontContext.vspacing;
			dx = 0;
		}

		immf_glyph *Glyph = &immf_GlobalGlyphs.Glyph[(int)c];
		_immf_gl_DrawGlyph(dx + x, dy + y, w, h, Glyph->gl_TexId);

		dx += w + immf_GlobalImmFontContext.hspacing;
	}

	glPopAttrib();
	glPopMatrix();
}


void immf_DrawString(float x, float y, float w, float h, immf__u32 color, const char *fmt, ...)
{
	static char buf[IMMF_DRAW_STRING_BUF_MAX];
	va_list vl;
	va_start(vl, fmt);
	int bytesWritten = vsnprintf(buf, IMMF_DRAW_STRING_BUF_MAX - 1, fmt, vl);
	va_end(vl);

	if(bytesWritten < 1) return;

	buf[bytesWritten] = 0;
	immf_DrawStringBase(x, y, w, h, color, buf, bytesWritten);
}

#endif /* IMMF_IMPLEMENTATION */

#ifdef __cplusplus
}
#endif

#undef immf__u8
#undef immf__u32

#endif /* _IMMF_H_ */

/* 
   Changelog

    ====================
    2022-04-23
    ====================
    - Change all instances of enums to have the IMMF_ prefix. Previously, it was IMM_.
    - Address compiler warnings with `gcc -Wall -Wextra` (some are still remaining).
	




*/


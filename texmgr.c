#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <string.h>
#include <malloc.h>



typedef int bool;

typedef struct
{
	u32 *data;
	u32 width;
	u32 height;
	u32 bpp;

	// Internal use
	int _fallback;
} image;


// This function loads a fallback image if the requested file called by TEX_Load cannot be found
// It is a pink and black checkerboard pattern
image _TEX_LoadFallback()
{
	// 4x4 image, 4 bits per pixel

	image Image = {0};

	int w = 4;
	int h = 4;

	// Pink and black, respectively
	u32 p = 0xFFFF00FF;
	u32 b = 0x00000000;

	u32 top[4] = {p, p, b, b};
	u32 bot[4] = {b, b, p, p};

	// Note: According to the documentation, stbi_image_free calls free() internally
	// However, we do not want to rely on this behaviour for the deallocation of the data
	// The _fallback member in the image struct was added to indicate how to free the memory in TEX_LoadFromDisk
	u32 *data = calloc(4 * 4, sizeof *data);
	u32 *pixels = data;


	// Fill out top
	for(int i = 0; i < 2; i++)
	{
		memcpy(pixels, top, 4 * 4);
		pixels += 4;
	}

	// Fill out bottom
	for(int i = 0; i < 2; i++)
	{
		memcpy(pixels, bot, 4 * 4);
		pixels += 4;
	}


	Image.data = data;
	Image.width = w;
	Image.height = h;
	Image.bpp = 4;
	Image._fallback = 1;

	return Image;
}

image TEX_Load(const char *path, bool flip)
{
	image Image = {0};
	int w, h, channels;

	stbi_set_flip_vertically_on_load(flip);

	void *data = stbi_load(path, &w, &h, &channels, 4);

	if(data)
	{
		Image.data = data;
		Image.width = w;
		Image.height = h;
		Image.bpp = 4;
	}
	else
	{
		fprintf(stderr, "ERROR: Could not load image file '%s'. Using default fallback image\n", path);
		Image = _TEX_LoadFallback();
	}

	return Image;
}

enum
{
	TEX_FILTER_NEAREST = (1 << 0),
	TEX_FILTER_LINEAR = (1 << 1),
	TEX_CLAMP = (1 << 2),
};


u32 TEX_AllocateTexture(u32 width, u32 height, void *data, u32 flags)
{
	u32 id;
	glGenTextures(1, &id);

	u32 filter = GL_NEAREST;
	u32 wrap = GL_REPEAT;

	glBindTexture(GL_TEXTURE_2D, id);

	if(flags & TEX_FILTER_LINEAR) filter = GL_LINEAR;
	if(flags & TEX_FILTER_NEAREST) filter = GL_NEAREST;
	if(flags & TEX_CLAMP) wrap = GL_CLAMP_TO_EDGE;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	glBindTexture(GL_TEXTURE_2D, 0);
	return id;
}

void TEX_ImageFree(image *Image)
{
	if(!Image->_fallback)
	{
		stbi_image_free(Image->data);
	}
	else
	{
		free(Image->data);
	}
}

u32 TEX_AllocateTextureFromImage(image *Image, bool FreeAfterAllocation, u32 flags)
{
	u32 w = Image->width;
	u32 h = Image->height;
	void *data = Image->data;
	u32 texture = TEX_AllocateTexture(w, h, data, flags);

	if(FreeAfterAllocation)
	{
		TEX_ImageFree(Image);
	}

	return texture;
}


u32 TEX_LoadFromDisk(const char *path, bool flip, u32 flags)
{
	image Image = TEX_Load(path, flip);
	u32 texture = TEX_AllocateTextureFromImage(&Image, 1, flags);

	return texture;

}

#include <stdio.h>
#include <stdint.h>

#include <GL/gl.h>
#include <GLFW/glfw3.h>

#include "types.h"
#include "texmgr.c"


#define IMMF_IMPLEMENTATION
#include "immf_standalone.h"



typedef struct
{
	float offs_s; // Horizontal
	float offs_t; // Vertical
} texture_offset;

typedef struct
{
	float x;
	float y;
	float w;
	float h;
} rect;

// Globals
int Running = 1;
texture_offset TextureOffset = {0};
int DisplayHud = 1;
int DisplayHelp = 0;

int WrapModeIndex = 0;
int WrapModes[2] = {GL_REPEAT, GL_CLAMP};
int WrapModeShouldBeUpdated = 0;

u32 TexAwesomeface = 0;
u32 TexTile = 0;
u32 TexCurrent = 0;

void GLFW_ToggleFullscreen(GLFWwindow *_Window)
{
	struct windowplacement
	{
		int x;
		int y;
		int w;
		int h;
	};

	static struct windowplacement ws;
	static int initialized = 0;

	GLFWmonitor *_WindowMonitor = glfwGetWindowMonitor(_Window);
	const GLFWvidmode *_Vidmode = NULL;

	// Currently fullscreen, set to windowed
	if(_WindowMonitor)
	{
		// Init windowplacement struct
		if(!initialized)
		{
			_Vidmode = glfwGetVideoMode(_WindowMonitor);
			ws.w = _Vidmode->width / 2;
			ws.h = _Vidmode->height / 2;

			ws.x = (_Vidmode->width / 2) - (ws.w / 2);
			ws.y = (_Vidmode->height / 2) - (ws.h / 2);
		}

		// Maybe we should set the refresh rate explicitly
		glfwSetWindowMonitor(_Window, 0, ws.x, ws.y, ws.w, ws.h, GLFW_DONT_CARE);
	}
	// Currently windowed, set to fullscreen
	else
	{
		// Cache window placement
		glfwGetWindowPos(_Window, &ws.x, &ws.y);
		glfwGetWindowSize(_Window, &ws.w, &ws.h);

		// Fullscreen dimensions
		GLFWmonitor *_PrimaryMonitor = glfwGetPrimaryMonitor();
		_Vidmode = glfwGetVideoMode(_PrimaryMonitor);
		glfwSetWindowMonitor(_Window, _PrimaryMonitor, 0, 0, _Vidmode->width, _Vidmode->height, GLFW_DONT_CARE);

		// Hack
		// If we begin in windowed mode, this counts as an initialization
		initialized = 1;
	}
}


void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	if(action == GLFW_PRESS)
	{
		switch(key)
		{
			case GLFW_KEY_ESCAPE:
			case GLFW_KEY_Q:
				Running = 0;
			break;

			case GLFW_KEY_F:
				GLFW_ToggleFullscreen(Window);
			break;

			case GLFW_KEY_G:
				DisplayHud = !DisplayHud;
			break;

			case GLFW_KEY_H:
				DisplayHelp = !DisplayHelp;
			break;

			case GLFW_KEY_SPACE:
				WrapModeShouldBeUpdated = 1;
			break;

			// Select a texture
			case GLFW_KEY_1:
				TexCurrent = TexAwesomeface;
			break;

			case GLFW_KEY_2:
				TexCurrent = TexTile;
			break;

			// Reset texture offsets
			case GLFW_KEY_R:
				TextureOffset.offs_s = 0;
				TextureOffset.offs_t = 0;
			break;
		}
	}
}

void CbResize(GLFWwindow *Window, int w, int h)
{
	glViewport(0, 0, w, h);
}

void CbWindowClose(GLFWwindow *Window)
{
	Running = 0;
}

// Assumes texture is already bound
void GL_SetTextureWrapMode(GLenum mode)
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mode);
}

void GL_Rect(float x, float y, float w, float h, float r, float g, float b, float a)
{
	float x0 = x;
	float y0 = y;

	float x1 = x + w;
	float y1 = y + h;

	glBegin(GL_QUADS);

	glColor4f(r, g, b, a);
	glVertex2f(x0, y0);
	glVertex2f(x0, y1);
	glVertex2f(x1, y1);
	glVertex2f(x1, y0);

	glEnd();
}

rect MakeCenteredRect(float Right, float Bottom, float RectWidth, float RectHeight)
{
	rect Rect;
	Rect.x = (Right / 2) - (RectWidth / 2);
	Rect.y = (Bottom / 2) - (RectHeight / 2);
	Rect.w = RectWidth;
	Rect.h = RectHeight;

	return Rect;
}


int main(int argc, char **argv)
{
	glfwInit();
	GLFWwindow *Window = glfwCreateWindow(800, 600, "gl-texture-mapping", 0, 0);
	glfwMakeContextCurrent(Window);
	glfwSwapInterval(1);

	glfwSetKeyCallback(Window, CbKeys);
	glfwSetFramebufferSizeCallback(Window, CbResize);
	glfwSetWindowCloseCallback(Window, CbWindowClose);

	TexAwesomeface = TEX_LoadFromDisk("awesomeface.png", 1, TEX_FILTER_LINEAR);
	TexTile = TEX_LoadFromDisk("tile16x16.png", 1, TEX_FILTER_NEAREST);

	TexCurrent = TexAwesomeface;
	// TexCurrent = TexTile;



	// You can experiment with combinations of GL_CLAMP or GL_REPEAT in either S and T coordinates,
	// they don't necessarily need to be the same
	// GL_CLAMP: No wrapping
	// GL_REPEAT: Enable wrapping / repeating

	glBindTexture(GL_TEXTURE_2D, TexCurrent);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glClearColor(0, 0, 0, 1);

	// Init immf
	float ImmfWidth = 800;
	float ImmfHeight = 600;
	immf_FilterMode(IMMF_FILTER_MODE_NONE);
	immf_Init(16, 16);
	immf_WindowDimensions(ImmfWidth, ImmfHeight);
	immf_VerticalFlip(1);
	immf_FontSpacingVert(16);

	float FrameTime = 0.0166;

	while(Running)
	{
		double FrameBegin = glfwGetTime();

		glClear(GL_COLOR_BUFFER_BIT);

		// Handle all input

		// Originally wrap_speed was hardcoded to 0.01 at a 60hz refresh rate
		// This is 0.01 / 0.016666
		float wrap_speed = 0.600 * FrameTime;

		// S
		if(glfwGetKey(Window, GLFW_KEY_D) == GLFW_PRESS)
		{
			TextureOffset.offs_s += wrap_speed;
		}

		if(glfwGetKey(Window, GLFW_KEY_A) == GLFW_PRESS)
		{
			TextureOffset.offs_s -= wrap_speed;
		}

		// T
		if(glfwGetKey(Window, GLFW_KEY_W) == GLFW_PRESS)
		{
			TextureOffset.offs_t += wrap_speed;
		}

		if(glfwGetKey(Window, GLFW_KEY_S) == GLFW_PRESS)
		{
			TextureOffset.offs_t -= wrap_speed;
		}

		if(WrapModeShouldBeUpdated)
		{
			WrapModeShouldBeUpdated = 0;

			WrapModeIndex += 1;
			WrapModeIndex %= 2;

			glBindTexture(GL_TEXTURE_2D, TexAwesomeface);
			GL_SetTextureWrapMode(WrapModes[WrapModeIndex]);

			glBindTexture(GL_TEXTURE_2D, TexTile);
			GL_SetTextureWrapMode(WrapModes[WrapModeIndex]);
		}

		// Begin drawing code

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// If you want to play around with the texture coordinates, this is the place to look
		// ------------------------------------------------------------------------------------------
		// Draw a fullscreen quad with a texture
		glBindTexture(GL_TEXTURE_2D, TexCurrent);

		// Try letting s0 and t0 begin at -10 instead of 0 and see what happens!
		// You'll get different behaviours based on the wrapping mode of the current texture
		float s0 = 0 + TextureOffset.offs_s;
		float t0 = 0 + TextureOffset.offs_t;
		float s1 = 1 + TextureOffset.offs_s;
		float t1 = 1 + TextureOffset.offs_t;

		float x0 = -1;
		float y0 = -1;
		float x1 = 1;
		float y1 = 1;

		glBegin(GL_QUADS);

		glColor3f(1, 1, 1);

		glTexCoord2f(s0, t0);
		glVertex2f(x0, y0);

		glTexCoord2f(s0, t1);
		glVertex2f(x0, y1);

		glTexCoord2f(s1, t1);
		glVertex2f(x1, y1);

		glTexCoord2f(s1, t0);
		glVertex2f(x1, y0);

		glEnd();
		// ------------------------------------------------------------------------------------------

		if(DisplayHud)
		{
			// Background box for the text
			glPushMatrix();
			glLoadIdentity();
			glDisable(GL_TEXTURE_2D);
			glOrtho(0, ImmfWidth, ImmfHeight, 0, 0, 1);

			float w = 300;
			float h = 80;
			GL_Rect(0, 0, w, h, 0.2, 0.2, 0.2, 0.75);

			glPopMatrix();

			// Display the current texture coordinate offsets
			immf_DrawString(10, 10, 20, 20, 0xFFFFFFFF,
					"OFF S: %.4f\nOFF T: %.4f", TextureOffset.offs_s, TextureOffset.offs_t);
		}

		if(DisplayHelp)
		{
			// Background box for the text
			glPushMatrix();
			glLoadIdentity();
			glDisable(GL_TEXTURE_2D);
			glOrtho(0, ImmfWidth, ImmfHeight, 0, 0, 1);

			rect Rect = MakeCenteredRect(ImmfWidth, ImmfHeight, 600, 320);

			// Outer
			GL_Rect(Rect.x, Rect.y, Rect.w, Rect.h, 1, 1, 1, 1);

			// Inner
			float offx = 20;
			float offy = 15;
			GL_Rect(Rect.x + offx, Rect.y + offy, Rect.w - (offx * 2), Rect.h - (offy * 2), 0, 0, 0, 1);

			glPopMatrix();

			float TextX = Rect.x + offx + 10;
			float TextY = Rect.y + offy + 10;

			// Display help text
			immf_PushAttrib(IMMF_ATTRIB_SPACING_VERT);
			immf_FontSpacingVert(12);
			immf_DrawString(TextX, TextY, 14, 14, 0xFFFFFFFF,
					"%s",
					"Help\n"
					"1       Select awesomeface texture\n"
					"2       Select tile texture\n"
					"SPACE   Toggle texture wrapping mode\n"
					"R       Reset texture offsets\n"
					"G       Toggle texture offset display\n"
					"F       Toggle fullscreen\n"
					"Q/ESC   Quit\n"
					"H       Toggle this help\n"
					"\n"
					"Written in 2023.04 by Z"
					"");

			immf_PopAttrib();
		}

		// End drawing code


		glfwSwapBuffers(Window);
		glfwPollEvents();

		double FrameEnd = glfwGetTime();

		FrameTime = (FrameEnd - FrameBegin);
	}

	glfwTerminate();

	return 0;
}
